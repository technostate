#!/usr/bin/perl -Tw
# $Id: sets.cgi,v 1.7 1999-07-18 05:08:29 ivan Exp $
# Copyright (c) 1999 Ivan Kohler.  All rights reserved.
# This program is free software; you can redistribute it and/or modify it under
# the same terms as perl itself

use strict;
use vars qw ( $data_source $user $password $table @fields
              $cgi $dbh
            );
use subs qw( print_form );
use CGI qw(tr th td);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

$data_source = "DBI:mysql:technostate";
$user = "agent";
$password = "t3chno";

$table = "SETS";
@fields = qw( SET_ID SETSTART DURATION FILENAME FILESIZE DESCRIPTION INFO KEYWORDS DOWNLOADS SHOW_ID );

$cgi = new CGI;

$dbh = DBI->connect( $data_source, $user, $password )
  or die "Cannot connect: ". $DBI::errstr;

unless ( $cgi->param('magic') ) { #first time through

  my $sth = $dbh->prepare( "SELECT * FROM $table" )
    or die $dbh->errstr;
  my $rv = $sth->execute;
  die $sth->errstr unless $rv;

  print $cgi->header( '-expires' => 'now' ),
        $cgi->start_html('Set listing'),
        $cgi->h1('Set listing'),
  ;

  unless ( $sth eq '0E0' ) {

    my @columns = @{ $sth->{'NAME'} };

    print $cgi->start_table,
          $cgi->tr(
            ( map { $cgi->th($_) } @columns ),
            $cgi->th('People')
          )
    ;

    my %hash = ();
    my $hashref = undef;
    while ( $hashref = $sth->fetchrow_hashref ) {
      %hash = %{$hashref};
      print $cgi->tr(
        ( map { $cgi->td( $hash{$_} ) } @columns ) ),
        ;
    }
    print $cgi->end_table;
  }

  $cgi->param('magic', 'new_form');
  print '<P><A HREF="', $cgi->self_url, '">Add new set</A>';
  print $cgi->end_html;

  exit;

} elsif ( $cgi->param('magic') eq 'new_form' ) {
  $cgi->param('SET_ID', 0);
  $cgi->param('magic', 'process_form');
  &print_form( $cgi, "Add set" );
  exit;
} elsif ( $cgi->param('magic') eq 'process_form' ) {

  $cgi->param('FILESIZE', 0);
  $cgi->param('DOWNLOADS', 0);

  my $field;
  foreach $field ( @fields ) {
    if ( $cgi->param( $field ) ) {
      $cgi->param( $field ) =~ /^(.*)$/;
      my $param = $1 || 0;
      if ( (DBI::looks_like_number($param))[0] ) {
        $cgi->param( $field, $param );
      } else {
        $cgi->param( $field, $dbh->quote($param) );
      }
    }
  }
  
  my $statement = "INSERT INTO $table ( ".
                  join(', ', @fields ).
                  ' ) VALUES ( '.
                  join( ', ', map { $cgi->param($_) } @fields ).
                  ' )'
  ;
  warn $statement;
  my $sth = $dbh->prepare($statement)
    or die $dbh->errstr;
  my $rv = $sth->execute;
  die $sth->errstr unless $rv;

  my $set_id = $sth->{'insertid'};
  warn $set_id;

  $dbh->do( "DELETE FROM PERSONS_SETS WHERE ( SET_ID = $set_id )" )
    or die $dbh->errstr;

  my $person_id;
  foreach $person_id ( $cgi->param('PERSON_ID') ) {
    $dbh->do(
      "INSERT INTO PERSONS_SETS ( PERSON_ID, SET_ID ) ".
      "VALUES ( $person_id, $set_id )"
    ) or die $dbh->errstr;
  }
  
  #my $rv = $sth->execute;
  #die $sth->errstr unless $rv;

  my $url = $cgi->url;
  $url =~ s/^\/[\/]+$//;
  print $cgi->redirect($url);

}

sub print_form {
  my $cgi = shift;
  my $action = shift;

  my %persons = map { @{$_}; }
         @{$dbh->selectall_arrayref( "SELECT PERSON_ID, NAME FROM PERSONS" )};
  print $cgi->header,
        $cgi->start_html($action),
        $cgi->h1($action),
        $cgi->start_form,
        $cgi->hidden( -name => 'SET_ID' ),
        "Start Time: ", $cgi->textfield( -name => 'SETSTART' ), "<BR>", 
        "Length: ", $cgi->textfield( -name => 'DURATION' ), "<BR>",
        "Filename: ", $cgi->textfield( -name => 'FILENAME' ), "<BR>",
        "Short Description: ", $cgi->textarea( -name => 'DESCRIPTION', -cols => '45', -rows => '2' ), "<BR>",
        "Long Description: ", $cgi->textarea( -name => 'INFO', -cols => '45', -rows => '5' ), "<BR>",
        "Keywords: ", $cgi->textfield( -name => 'KEYWORDS' ), "<BR>",
        "People: ", $cgi->scrolling_list(
          -name => "PERSON_ID",
          '-values' => [ sort { $persons{$a} cmp $persons{$b} } keys(%persons) ],
          '-labels' => \%persons,
          '-multiple' => 'true',
        ),
        "Show?: ", $cgi->textfield( -name => 'SHOW_ID' ), "<BR>",
        $cgi->hidden( -name => 'magic'),
        $cgi->submit('Submit'),
        $cgi->end_form,
        $cgi->end_html;
  ;
}
